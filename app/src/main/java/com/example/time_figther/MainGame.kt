package com.example.time_figther

import android.app.PendingIntent
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.firebase.firestore.FirebaseFirestore


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainGame.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainGame.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainGame : Fragment() {
    // TODO: Rename and change types of parameters


    internal lateinit var welcomMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    val db =FirebaseFirestore.getInstance()
    var name=""

    @State //variables que queremos que se guarden en el estado
    var score = 0

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 100
    private lateinit var countDownTimer: CountDownTimer


    @State
    var timeleft = 10
    @State
    var gameStarted = false




    private val args: MainGameArgs by navArgs()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        StateSaver.restoreInstanceState(this, savedInstanceState)

        timeLeftTextView = view.findViewById(R.id.time_left)
        gameScoreTextView = view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)

        /*if (savedInstanceState != null){
            score=savedInstanceState.getInt(SCORE_KEY)
        }
        gameScoreTextView.text=getString(R.string.score_i,score)*/

        welcomMessage = view.findViewById(R.id.welcome_message)
        val playerName = args.playerName
        name=playerName
        // funcion get strgin representa a mi resources y luego busco el que cree para el mensaje
        //id de mi string y reemplazo %s lo que hace to String
        welcomMessage.text = getString(
            R.string.welcome_player, playerName.toString()
        )


        goButton.setOnClickListener { incrementScore() }
        resetGame()

        if(gameStarted){
            resetGame()
            return
        }
    }

    fun incrementScore() {
        if (!gameStarted) {
            startGame()
        }
        score++
        gameScoreTextView.text = getString(R.string.score_i, score)

    }


    private fun resetGame() {
        gameScoreTextView.text = getString(R.string.score_i, score)

        score = 0
        gameScoreTextView.text = getString(R.string.score_i, score)

        timeleft = 10
        timeLeftTextView.text = getString(R.string.time_left_i, timeleft)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_i, timeleft)
            }
        }

        gameStarted = false
    }


    private fun startGame() {
        if (!gameStarted) {
            countDownTimer.start()
            gameStarted = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    companion object {
        private val SCORE_KEY = "SCORE"
    }


    fun restoreGame() {

        gameScoreTextView.text = getString(R.string.score_i,score)

        countDownTimer = object : CountDownTimer
            (timeleft*1000L, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeleft=millisUntilFinished.toInt()/1000
                timeLeftTextView.text=getString(R.string.time_left_i,timeleft)
            }

            override fun onFinish() {

            }

        }

        countDownTimer.start()
    }

    private fun endGame(){
        Toast.makeText(
            activity,
            getString(R.string.end_game,score), //el activity relacionado con este fragment si tenemos un activity solo ponemos this
            Toast.LENGTH_LONG).show()

        val data=HashMap<String,Any>()
        data["name"]=name
        data["Score"]=score
        db.collection("players")
            .add(data)
            .addOnSuccessListener{documentReference ->
                Log.d("MAIN GAME","DocumentSnapshot added with ID: ${documentReference}")
            }
            .addOnFailureListener{e->
                Log.w("","Error adding Document", e)
            }
        resetGame()
    }



}


